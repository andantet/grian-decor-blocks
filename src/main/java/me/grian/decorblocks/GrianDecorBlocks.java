package me.grian.decorblocks;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
// import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Block;
// import net.minecraft.item.BlockItem;
// import net.minecraft.item.Item;
// import net.minecraft.item.ItemGroup;
// import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class GrianDecorBlocks implements ModInitializer {
    private static final String MOD_ID = "griandecorblocks";
    // public static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.build(new Identifier(MOD_ID, "item_group"),
    //         () -> new ItemStack(TEST_BLOCK));
    
    // public static final Block TEST_BLOCK = registerBlock(Blocks.VANILLA_BLOCK);
    // public static final Block UWU_BLOCK = new CustomClassBlock(FabricBlockSettings.copy(Blocks.VANILLA_BLOCK).build());

    @Override
	public void onInitialize() {
		System.out.println("[Grian's Decoration Blocks] Loaded");
    }

    static void initialiseBlocks() {
        // register("test_block", TEST_BLOCK);
        // register("uwu_block", UWU_BLOCK);
    }
    
    static void register(String id, Block block) {
        Registry.register(Registry.BLOCK, new Identifier(MOD_ID, id), block);
        // Registry.register(Registry.ITEM, new Identifier(MOD_ID, id),
        //         new BlockItem(block, new Item.Settings().group(ITEM_GROUP)));
    }

    public static Block registerBlock(Block copyBlock) {
        return new Block(FabricBlockSettings.copy(copyBlock).build());
    }
}
