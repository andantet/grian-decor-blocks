# Grian's Decoration Blocks

[![](https://img.shields.io/badge/api-fabric-orange.svg)](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files)

Grian's Decoration Blocks is a simple Minecraft mod that adds a few decoration blocks.

## Building
1. JDK 8 is required. Install it using https://adoptopenjdk.net
2. Download the [sources](https://gitlab.com/AndanteDevs/grian-decor-blocks/-/archive/master/grian-decor-blocks-master.zip) to a folder
2. Open a terminal window in the same directory as the sources (`shift` + `right click` while inside the desired folder and `Open PowerShell window here`) and run `./gradlew build`
3. After some time, the built mod will be in `/build/libs`.

## Installation (Users)
Grian's Decoration Blocks uses *Fabric* as it's mod API. Refer to their installation instructions [here](https://fabricmc.net#installation)

Once you have Fabric installed, simply download the latest version of Grian's Decoration Blocks from [CurseForge](https://curseforge.com/minecraft/mc-mods/grian-decor-blocks/files) and place it in your `mods` folder.
**Remember to use the Fabric launcher profile when starting the game!**
